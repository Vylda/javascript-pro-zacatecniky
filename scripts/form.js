const dom = {
  form: document.querySelector('form'),
  output: document.querySelector('#output'),
};

const submitFormHandler = async (event) => {
  event.preventDefault();

  const data = {
    jmeno: dom.form['jmeno'].value,
    prijmeni: dom.form['prijmeni'].value,
    mail: dom.form['mail'].value,
    souhlas: dom.form['souhlas'].checked,
  };

  const query = new URLSearchParams(data);

  const config = {
    body: query,
    method: 'POST',
  };
  const url = 'https://jsdev.cz/xhr/';

  const start = new Date();
  const response = await fetch(url, config);
  const end = new Date();
  console.log('Fetching time:', end - start);

  if (!response.ok) {
    dom.output.textContent = 'Nepovedlo se mi to odeslat';
    return;
  }

  dom.output.textContent = await response.text();
};

dom.form.addEventListener('submit', submitFormHandler);
