const now = new Date();

console.log(now);
console.log(now.toLocaleDateString());
console.log(now.toLocaleTimeString());

const datePrefs = {
  day: 'numeric',
  month: 'long',
  year: 'numeric',
};

console.log(now.toLocaleDateString('cs', datePrefs));

const birthDay = new Date(2006, 9, 13);
console.log(birthDay.getDay());

const getMonthLength = (year, month) => {
  const lastDay = new Date(year, month + 1, 0);

  return lastDay.getDate();
};

console.log('2024, 9', getMonthLength(2024, 8));
console.log('2024, 2', getMonthLength(2024, 1));
console.log('2022, 2', getMonthLength(2022, 1));

console.log(navigator.language);
console.log(navigator.languages);

const getLocalTime = () => {
  const now = new Date();
  return now.toLocaleTimeString(navigator.language);
};

const showClock = () => {
  const localTime = getLocalTime();
  document.title = localTime;
};

const runClock = () => {
  showClock();
  setInterval(showClock, 1000);
};

runClock();
